package com.sdn.app.service;

import com.sdn.app.service.dto.SdnlistDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Sdnlist.
 */
public interface SdnlistService {

    /**
     * Save a sdnlist.
     *
     * @param sdnlistDTO the entity to save
     * @return the persisted entity
     */
    SdnlistDTO save(SdnlistDTO sdnlistDTO);

    /**
     * Get all the sdnlists.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<SdnlistDTO> findAll(Pageable pageable);


    /**
     * Get the "id" sdnlist.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<SdnlistDTO> findOne(Long id);

    /**
     * Delete the "id" sdnlist.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
    
    /**
     * Download the .txt file from ftp address
     * @param serverAddress the server address
     * @param userName connection user name
     * @param password connection password
     * @param remoteFilePath the remote file path
     * @param localFile le local file generated
     * @param port usually is 21
     */
    void ftpDownloadFile(String serverAddress, String userName, String password, String remoteFilePath, String localFile, int port);
    
    /**
     * Persist each sdnList with the sdn name in the database
     * @param localFilePath the local file path to retrieve the file after dowloading
     */
    void insertNames(String localFilePath);
}
