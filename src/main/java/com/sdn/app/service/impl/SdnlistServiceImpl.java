package com.sdn.app.service.impl;

import com.sdn.app.service.SdnlistService;
import com.sdn.app.domain.Sdnlist;
import com.sdn.app.repository.SdnlistRepository;
import com.sdn.app.service.dto.SdnlistDTO;
import com.sdn.app.service.mapper.SdnlistMapper;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Sdnlist.
 */
@Service
@Transactional
public class SdnlistServiceImpl implements SdnlistService {

	private final Logger log = LoggerFactory.getLogger(SdnlistServiceImpl.class);

	private final SdnlistRepository sdnlistRepository;

	private final SdnlistMapper sdnlistMapper;

	public SdnlistServiceImpl(SdnlistRepository sdnlistRepository, SdnlistMapper sdnlistMapper) {
		this.sdnlistRepository = sdnlistRepository;
		this.sdnlistMapper = sdnlistMapper;
	}

	/**
	 * Save a sdnlist.
	 *
	 * @param sdnlistDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	@Override
	public SdnlistDTO save(SdnlistDTO sdnlistDTO) {
		log.debug("Request to save Sdnlist : {}", sdnlistDTO);
		Sdnlist sdnlist = sdnlistMapper.toEntity(sdnlistDTO);
		sdnlist = sdnlistRepository.save(sdnlist);
		return sdnlistMapper.toDto(sdnlist);
	}

	/**
	 * Get all the sdnlists.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<SdnlistDTO> findAll(Pageable pageable) {
		log.debug("Request to get all Sdnlists");
		return sdnlistRepository.findAll(pageable).map(sdnlistMapper::toDto);
	}

	/**
	 * Get one sdnlist by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<SdnlistDTO> findOne(Long id) {
		log.debug("Request to get Sdnlist : {}", id);
		return sdnlistRepository.findById(id).map(sdnlistMapper::toDto);
	}

	/**
	 * Delete the sdnlist by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete Sdnlist : {}", id);
		sdnlistRepository.deleteById(id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void ftpDownloadFile(String serverAddress, String userName, String password, String remoteFilePath,
			String localFile, int port) {
		log.debug("Executing ftpDownloadFile");
		FTPClient ftpClient = new FTPClient();
		try {

			ftpClient.connect(serverAddress, port);
			ftpClient.login(userName, password);
			ftpClient.enterLocalPassiveMode();
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

			File localfile = new File("E:/ftpServerFile.txt");
			FileOutputStream outputStream = new FileOutputStream(localfile);
			boolean success = ftpClient.retrieveFile(remoteFilePath, outputStream);
			outputStream.close();

			if (success) {
				System.out.println("Ftp file successfully download.");
			}

		} catch (IOException ex) {
			System.out.println("Error occurs in downloading files from ftp Server : " + ex.getMessage());
		} finally {
			try {
				if (ftpClient.isConnected()) {
					ftpClient.logout();
					ftpClient.disconnect();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void insertNames(String localFilePath) {
		log.debug("Executing insertNames");
		 String contents = "";
			try {
				contents = Files.lines(Paths.get(localFilePath)).collect(Collectors.joining("\n"));
			} catch (IOException e) {
			
				e.printStackTrace();
			}
	        
	        String[] paragraphs = contents.split("\\n{2}");
	        Pattern pattern = Pattern.compile(".*a\\.k\\.a. *(.*);");
	        Matcher matcher ;
	        String sdnName ;
	        SdnlistDTO sdnlistDTO = new SdnlistDTO();
	        for (String paragraph : paragraphs) {
		        matcher = pattern.matcher(paragraph);
		        if (matcher.find()){
		        	sdnName = matcher.group(1);
		        	sdnlistDTO.setSdnName(sdnName);
		        	save(sdnlistDTO);
		        }
	        }
		
	}
}
