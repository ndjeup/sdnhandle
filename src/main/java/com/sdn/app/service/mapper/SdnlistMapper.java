package com.sdn.app.service.mapper;

import com.sdn.app.domain.*;
import com.sdn.app.service.dto.SdnlistDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Sdnlist and its DTO SdnlistDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SdnlistMapper extends EntityMapper<SdnlistDTO, Sdnlist> {



    default Sdnlist fromId(Long id) {
        if (id == null) {
            return null;
        }
        Sdnlist sdnlist = new Sdnlist();
        sdnlist.setId(id);
        return sdnlist;
    }
}
