package com.sdn.app.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Sdnlist entity.
 */
public class SdnlistDTO implements Serializable {

    private Long id;

    private String sdnName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSdnName() {
        return sdnName;
    }

    public void setSdnName(String sdnName) {
        this.sdnName = sdnName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SdnlistDTO sdnlistDTO = (SdnlistDTO) o;
        if (sdnlistDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sdnlistDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SdnlistDTO{" +
            "id=" + getId() +
            ", sdnName='" + getSdnName() + "'" +
            "}";
    }
}
