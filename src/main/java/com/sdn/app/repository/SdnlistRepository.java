package com.sdn.app.repository;

import com.sdn.app.domain.Sdnlist;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Sdnlist entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SdnlistRepository extends JpaRepository<Sdnlist, Long> {

}
