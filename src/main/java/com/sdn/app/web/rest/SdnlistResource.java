package com.sdn.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.sdn.app.service.SdnlistService;
import com.sdn.app.web.rest.errors.BadRequestAlertException;
import com.sdn.app.web.rest.util.HeaderUtil;
import com.sdn.app.web.rest.util.PaginationUtil;
import com.sdn.app.service.dto.SdnlistDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Sdnlist.
 */
@RestController
@RequestMapping("/api")
public class SdnlistResource {

    private final Logger log = LoggerFactory.getLogger(SdnlistResource.class);

    private static final String ENTITY_NAME = "sdnlist";

    private final SdnlistService sdnlistService;

    public SdnlistResource(SdnlistService sdnlistService) {
        this.sdnlistService = sdnlistService;
    }

    /**
     * POST  /sdnlists : Create a new sdnlist.
     *
     * @param sdnlistDTO the sdnlistDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sdnlistDTO, or with status 400 (Bad Request) if the sdnlist has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sdnlists")
    @Timed
    public ResponseEntity<SdnlistDTO> createSdnlist(@RequestBody SdnlistDTO sdnlistDTO) throws URISyntaxException {
        log.debug("REST request to save Sdnlist : {}", sdnlistDTO);
        if (sdnlistDTO.getId() != null) {
            throw new BadRequestAlertException("A new sdnlist cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SdnlistDTO result = sdnlistService.save(sdnlistDTO);
        return ResponseEntity.created(new URI("/api/sdnlists/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sdnlists : Updates an existing sdnlist.
     *
     * @param sdnlistDTO the sdnlistDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sdnlistDTO,
     * or with status 400 (Bad Request) if the sdnlistDTO is not valid,
     * or with status 500 (Internal Server Error) if the sdnlistDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sdnlists")
    @Timed
    public ResponseEntity<SdnlistDTO> updateSdnlist(@RequestBody SdnlistDTO sdnlistDTO) throws URISyntaxException {
        log.debug("REST request to update Sdnlist : {}", sdnlistDTO);
        if (sdnlistDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SdnlistDTO result = sdnlistService.save(sdnlistDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sdnlistDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sdnlists : get all the sdnlists.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of sdnlists in body
     */
    @GetMapping("/sdnlists")
    @Timed
    public ResponseEntity<List<SdnlistDTO>> getAllSdnlists(Pageable pageable) {
        log.debug("REST request to get a page of Sdnlists");
        Page<SdnlistDTO> page = sdnlistService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sdnlists");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /sdnlists/:id : get the "id" sdnlist.
     *
     * @param id the id of the sdnlistDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sdnlistDTO, or with status 404 (Not Found)
     */
    @GetMapping("/sdnlists/{id}")
    @Timed
    public ResponseEntity<SdnlistDTO> getSdnlist(@PathVariable Long id) {
        log.debug("REST request to get Sdnlist : {}", id);
        Optional<SdnlistDTO> sdnlistDTO = sdnlistService.findOne(id);
        return ResponseUtil.wrapOrNotFound(sdnlistDTO);
    }

    /**
     * DELETE  /sdnlists/:id : delete the "id" sdnlist.
     *
     * @param id the id of the sdnlistDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sdnlists/{id}")
    @Timed
    public ResponseEntity<Void> deleteSdnlist(@PathVariable Long id) {
        log.debug("REST request to delete Sdnlist : {}", id);
        sdnlistService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
