/**
 * View Models used by Spring MVC REST controllers.
 */
package com.sdn.app.web.rest.vm;
