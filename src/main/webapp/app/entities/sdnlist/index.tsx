import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Sdnlist from './sdnlist';
import SdnlistDetail from './sdnlist-detail';
import SdnlistUpdate from './sdnlist-update';
import SdnlistDeleteDialog from './sdnlist-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={SdnlistUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={SdnlistUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={SdnlistDetail} />
      <ErrorBoundaryRoute path={match.url} component={Sdnlist} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={SdnlistDeleteDialog} />
  </>
);

export default Routes;
