import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ISdnlist, defaultValue } from 'app/shared/model/sdnlist.model';

export const ACTION_TYPES = {
  FETCH_SDNLIST_LIST: 'sdnlist/FETCH_SDNLIST_LIST',
  FETCH_SDNLIST: 'sdnlist/FETCH_SDNLIST',
  CREATE_SDNLIST: 'sdnlist/CREATE_SDNLIST',
  UPDATE_SDNLIST: 'sdnlist/UPDATE_SDNLIST',
  DELETE_SDNLIST: 'sdnlist/DELETE_SDNLIST',
  RESET: 'sdnlist/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ISdnlist>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type SdnlistState = Readonly<typeof initialState>;

// Reducer

export default (state: SdnlistState = initialState, action): SdnlistState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_SDNLIST_LIST):
    case REQUEST(ACTION_TYPES.FETCH_SDNLIST):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_SDNLIST):
    case REQUEST(ACTION_TYPES.UPDATE_SDNLIST):
    case REQUEST(ACTION_TYPES.DELETE_SDNLIST):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_SDNLIST_LIST):
    case FAILURE(ACTION_TYPES.FETCH_SDNLIST):
    case FAILURE(ACTION_TYPES.CREATE_SDNLIST):
    case FAILURE(ACTION_TYPES.UPDATE_SDNLIST):
    case FAILURE(ACTION_TYPES.DELETE_SDNLIST):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_SDNLIST_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_SDNLIST):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_SDNLIST):
    case SUCCESS(ACTION_TYPES.UPDATE_SDNLIST):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_SDNLIST):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/sdnlists';

// Actions

export const getEntities: ICrudGetAllAction<ISdnlist> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_SDNLIST_LIST,
    payload: axios.get<ISdnlist>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<ISdnlist> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_SDNLIST,
    payload: axios.get<ISdnlist>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<ISdnlist> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_SDNLIST,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ISdnlist> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_SDNLIST,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<ISdnlist> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_SDNLIST,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
