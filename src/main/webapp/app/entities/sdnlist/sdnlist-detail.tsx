import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './sdnlist.reducer';
import { ISdnlist } from 'app/shared/model/sdnlist.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ISdnlistDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: number }> {}

export class SdnlistDetail extends React.Component<ISdnlistDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { sdnlistEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            Sdnlist [<b>{sdnlistEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="sdnName">Sdn Name</span>
            </dt>
            <dd>{sdnlistEntity.sdnName}</dd>
          </dl>
          <Button tag={Link} to="/entity/sdnlist" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/sdnlist/${sdnlistEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ sdnlist }: IRootState) => ({
  sdnlistEntity: sdnlist.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SdnlistDetail);
