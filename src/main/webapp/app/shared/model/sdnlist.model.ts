export interface ISdnlist {
  id?: number;
  sdnName?: string;
}

export const defaultValue: Readonly<ISdnlist> = {};
