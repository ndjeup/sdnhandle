package com.sdn.app.web.rest;

import com.sdn.app.SdnApp;

import com.sdn.app.domain.Sdnlist;
import com.sdn.app.repository.SdnlistRepository;
import com.sdn.app.service.SdnlistService;
import com.sdn.app.service.dto.SdnlistDTO;
import com.sdn.app.service.mapper.SdnlistMapper;
import com.sdn.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.sdn.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SdnlistResource REST controller.
 *
 * @see SdnlistResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SdnApp.class)
public class SdnlistResourceIntTest {

    private static final String DEFAULT_SDN_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SDN_NAME = "BBBBBBBBBB";

    @Autowired
    private SdnlistRepository sdnlistRepository;


    @Autowired
    private SdnlistMapper sdnlistMapper;
    

    @Autowired
    private SdnlistService sdnlistService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSdnlistMockMvc;

    private Sdnlist sdnlist;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SdnlistResource sdnlistResource = new SdnlistResource(sdnlistService);
        this.restSdnlistMockMvc = MockMvcBuilders.standaloneSetup(sdnlistResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sdnlist createEntity(EntityManager em) {
        Sdnlist sdnlist = new Sdnlist()
            .sdnName(DEFAULT_SDN_NAME);
        return sdnlist;
    }

    @Before
    public void initTest() {
        sdnlist = createEntity(em);
    }

    @Test
    @Transactional
    public void createSdnlist() throws Exception {
        int databaseSizeBeforeCreate = sdnlistRepository.findAll().size();

        // Create the Sdnlist
        SdnlistDTO sdnlistDTO = sdnlistMapper.toDto(sdnlist);
        restSdnlistMockMvc.perform(post("/api/sdnlists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sdnlistDTO)))
            .andExpect(status().isCreated());

        // Validate the Sdnlist in the database
        List<Sdnlist> sdnlistList = sdnlistRepository.findAll();
        assertThat(sdnlistList).hasSize(databaseSizeBeforeCreate + 1);
        Sdnlist testSdnlist = sdnlistList.get(sdnlistList.size() - 1);
        assertThat(testSdnlist.getSdnName()).isEqualTo(DEFAULT_SDN_NAME);
    }

    @Test
    @Transactional
    public void createSdnlistWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sdnlistRepository.findAll().size();

        // Create the Sdnlist with an existing ID
        sdnlist.setId(1L);
        SdnlistDTO sdnlistDTO = sdnlistMapper.toDto(sdnlist);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSdnlistMockMvc.perform(post("/api/sdnlists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sdnlistDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Sdnlist in the database
        List<Sdnlist> sdnlistList = sdnlistRepository.findAll();
        assertThat(sdnlistList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSdnlists() throws Exception {
        // Initialize the database
        sdnlistRepository.saveAndFlush(sdnlist);

        // Get all the sdnlistList
        restSdnlistMockMvc.perform(get("/api/sdnlists?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sdnlist.getId().intValue())))
            .andExpect(jsonPath("$.[*].sdnName").value(hasItem(DEFAULT_SDN_NAME.toString())));
    }
    

    @Test
    @Transactional
    public void getSdnlist() throws Exception {
        // Initialize the database
        sdnlistRepository.saveAndFlush(sdnlist);

        // Get the sdnlist
        restSdnlistMockMvc.perform(get("/api/sdnlists/{id}", sdnlist.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(sdnlist.getId().intValue()))
            .andExpect(jsonPath("$.sdnName").value(DEFAULT_SDN_NAME.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingSdnlist() throws Exception {
        // Get the sdnlist
        restSdnlistMockMvc.perform(get("/api/sdnlists/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSdnlist() throws Exception {
        // Initialize the database
        sdnlistRepository.saveAndFlush(sdnlist);

        int databaseSizeBeforeUpdate = sdnlistRepository.findAll().size();

        // Update the sdnlist
        Sdnlist updatedSdnlist = sdnlistRepository.findById(sdnlist.getId()).get();
        // Disconnect from session so that the updates on updatedSdnlist are not directly saved in db
        em.detach(updatedSdnlist);
        updatedSdnlist
            .sdnName(UPDATED_SDN_NAME);
        SdnlistDTO sdnlistDTO = sdnlistMapper.toDto(updatedSdnlist);

        restSdnlistMockMvc.perform(put("/api/sdnlists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sdnlistDTO)))
            .andExpect(status().isOk());

        // Validate the Sdnlist in the database
        List<Sdnlist> sdnlistList = sdnlistRepository.findAll();
        assertThat(sdnlistList).hasSize(databaseSizeBeforeUpdate);
        Sdnlist testSdnlist = sdnlistList.get(sdnlistList.size() - 1);
        assertThat(testSdnlist.getSdnName()).isEqualTo(UPDATED_SDN_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingSdnlist() throws Exception {
        int databaseSizeBeforeUpdate = sdnlistRepository.findAll().size();

        // Create the Sdnlist
        SdnlistDTO sdnlistDTO = sdnlistMapper.toDto(sdnlist);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restSdnlistMockMvc.perform(put("/api/sdnlists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sdnlistDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Sdnlist in the database
        List<Sdnlist> sdnlistList = sdnlistRepository.findAll();
        assertThat(sdnlistList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSdnlist() throws Exception {
        // Initialize the database
        sdnlistRepository.saveAndFlush(sdnlist);

        int databaseSizeBeforeDelete = sdnlistRepository.findAll().size();

        // Get the sdnlist
        restSdnlistMockMvc.perform(delete("/api/sdnlists/{id}", sdnlist.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Sdnlist> sdnlistList = sdnlistRepository.findAll();
        assertThat(sdnlistList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Sdnlist.class);
        Sdnlist sdnlist1 = new Sdnlist();
        sdnlist1.setId(1L);
        Sdnlist sdnlist2 = new Sdnlist();
        sdnlist2.setId(sdnlist1.getId());
        assertThat(sdnlist1).isEqualTo(sdnlist2);
        sdnlist2.setId(2L);
        assertThat(sdnlist1).isNotEqualTo(sdnlist2);
        sdnlist1.setId(null);
        assertThat(sdnlist1).isNotEqualTo(sdnlist2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SdnlistDTO.class);
        SdnlistDTO sdnlistDTO1 = new SdnlistDTO();
        sdnlistDTO1.setId(1L);
        SdnlistDTO sdnlistDTO2 = new SdnlistDTO();
        assertThat(sdnlistDTO1).isNotEqualTo(sdnlistDTO2);
        sdnlistDTO2.setId(sdnlistDTO1.getId());
        assertThat(sdnlistDTO1).isEqualTo(sdnlistDTO2);
        sdnlistDTO2.setId(2L);
        assertThat(sdnlistDTO1).isNotEqualTo(sdnlistDTO2);
        sdnlistDTO1.setId(null);
        assertThat(sdnlistDTO1).isNotEqualTo(sdnlistDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(sdnlistMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(sdnlistMapper.fromId(null)).isNull();
    }
}
